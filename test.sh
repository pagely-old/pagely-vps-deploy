#!/usr/bin/env bash

set -o errexit
set -o nounset
set -o pipefail

docker build -t pagely/vps-deploy .

if [[ -f .env ]]
then
    source .env
fi

docker run --rm \
    -e PAGELY_DEPLOY_DEST=$PAGELY_DEPLOY_DEST \
    -e PAGELY_INTEGRATION_SECRET=$PAGELY_INTEGRATION_SECRET \
    -e PAGELY_INTEGRATION_ID=$PAGELY_INTEGRATION_ID \
    -e INPUT_PAGELY_APP_ID=$PAGELY_APP_ID \
    -v "$(pwd)/test":/test \
    -w /test \
    pagely/vps-deploy
