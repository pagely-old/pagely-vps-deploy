# Bitbucket Pipelines Pipe:  Pagely Deploy to VPS

Deploy code from your build to a Pagely app.

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
  - pipe: pagely/pagely-vps-deploy:1
    variables:
      PAGELY_DEPLOY_DEST: $PAGELY_DEPLOY_DEST
      PAGELY_INTEGRATION_SECRET: $PAGELY_INTEGRATION_SECRET
      PAGELY_INTEGRATION_ID: $PAGELY_INTEGRATION_ID
      PAGELY_APP_ID: $PAGELY_APP_ID
      PAGELY_WORKING_DIR: $BITBUCKET_CLONE_DIR
```

## Variables

| Variable           | Usage                                                       |
| --------------------- | ----------------------------------------------------------- |
| PAGELY_DEPLOY_DEST (*)        | The filepath on your app for your build files to be put. |
| PAGELY_INTEGRATION_SECRET (*) | This secret is given when you create your integration. You should use a secure repository variable for this|
| PAGELY_INTEGRATION_ID (*)     | The integration ID found in Atomic. |
| PAGELY_APP_ID (*)             | The ID of the app you want to deploy to. |
| PAGELY_WORKING_DIR            | The directory whose contents we will deploy. Defaults to $PWD |

_(*) = required variable._

## Prerequisites

You must have an app and an integration set up in your Pagely account.

## Examples

```yaml
  - pipe: pagely/pagely-vps-deploy:1
    variables:
      PAGELY_DEPLOY_DEST: "/httpdocs/wp-content/plugins/myplugin"
      PAGELY_INTEGRATION_SECRET: $PAGELY_INTEGRATION_SECRET
      PAGELY_INTEGRATION_ID: $PAGELY_INTEGRATION_ID
      PAGELY_APP_ID: $PAGELY_APP_ID
      PAGELY_WORKING_DIR: $BITBUCKET_CLONE_DIR
```

## Support

Support tickets can be created from within the [Pagely Atomic](https://atomic.pagely.com) dashboard
